import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { IWorkout, IJournalEntry, IPost } from '../lib/model';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-workout-create',
  templateUrl: './workout-create.component.html',
  styleUrls: ['./workout-create.component.css']
})
export class WorkoutCreateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  	//import ("../../../library.js");
  	//@ts-ignore
  	populateLists();
  }
}
