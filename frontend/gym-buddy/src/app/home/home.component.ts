import { Component, OnInit } from '@angular/core';
import { WorkoutService } from '../services/workout.service';
import { IWorkout, IJournalEntry, IPost } from '../lib/model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  posts: IPost[] = [];

  constructor(private workoutService: WorkoutService) { }

  ngOnInit() {
    this.workoutService.updatePosts(true);
    this.workoutService.postsFriends$.subscribe(
      posts => {
        this.posts = posts;
        /*
        if (workouts.length < 6) {
          this.workoutsFew = workouts.slice(0, workouts.length + 1);
        } else {
          this.workoutsFew = workouts.slice(0, 6);
        }
        this.workouts = this.workoutsFew;
        */
      },
      err => {
      }
    );
  }

}
