import { Injectable } from '@angular/core';
import { IUser, IAccount, IProfile } from '../lib/model';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { ProfileService } from './profile.service';
import { config } from '../lib/config';

let mock: boolean = false;

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  loggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  userMap: any = {};

  constructor(private httpClient: HttpClient, private profileService: ProfileService) { }

  createAccount(account: IAccount): Observable<any> {
    account.email = account.email + "@purdue.edu";
    let user: IUser = {
      email: account.email,
      password: account.password1
    }
    let newUser: any = {
      name: account.name,
      email: account.email,
      password: account.password1
    }
    if (this.userMap[newUser.email]) {
      mockCreateAccountResponse.success = false;
    } else {
      mockCreateAccountResponse.success = true;
    }
    if (mockCreateAccountResponse.success == true) {
      this.userMap[newUser.email] = user;
    }
    if (mock) {
      return of(mockCreateAccountResponse);
    } else {
      //let url: 'http://gym-budy-server/account';
      let url: string = `${config.ServerUrl}/signup`;
      //debugger;
      return this.httpClient.post(url, newUser);
    }
  }

  login(user: IUser): Observable<any> {
    user.email = user.email + "@purdue.edu";
    if (this.userMap[user.email]) {
      if (this.userMap[user.email].password == user.password) {
        mockLoginResponse.success = true;
      } else {
        mockLoginResponse.success = false;
      }
    } else {
      mockLoginResponse.success = false;
    }
    if (mock) {
      return of(mockLoginResponse)
    } else {
      let url: string = `${config.ServerUrl}/login`;
      return this.httpClient.post(url, user);
    }
  }

}

let mockLoginResponse: any = {
  success: true,
  message: "test message"
}

const mockCreateAccountResponse: any = {
  success: true,
  message: "test message"
}
