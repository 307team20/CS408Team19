import { Injectable } from '@angular/core';
import { IProfile, IUser, IFriendRequest } from '../lib/model';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of, interval } from 'rxjs';
import { config } from '../lib/config';

let mock: boolean = false;

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  // initialize to mockUserProfile for testing without logging in otherwise null
  userProfile$: BehaviorSubject<IProfile> = new BehaviorSubject<IProfile>(mockUserProfile);

  constructor(private httpClient: HttpClient) {

    interval(3000).subscribe(val => {
      mockFriendRequests.push('rabbit');
    });

  }

  getOtherProfile(userName): Observable<IProfile> {
    if (mock) {
      // if (user.email === "jperson@purdue.edu") {
      //   return of(mockFriendUserProfile);
      // }
      // else if (user.email === "pete@purdue.edu") {
      //   return of(mockPrivateUserProfile);
      // }
      return of(mockPrivateUserProfile);
    } else {
      let url: string = `${config.ServerUrl}/viewAllBios?userName=${userName}&callType=private`;
      return this.httpClient.get<IProfile>(url);
    }
  }

  getProfile(user: IUser): Observable<IProfile> {
    if (mock) {
      return of(mockUserProfile);
    } else {
      //let url: string = `http://gym-budy-server/profile/${user.email}`;
      let un = user.email.split("@")[0];
      let url: string = `${config.ServerUrl}/viewAllBios?userName=${un}&callType=private`;
      return this.httpClient.get<IProfile>(url);
    }
  }

  createProfile(profile: IProfile): Observable<any> {
    console.log('create profile is running');
    if (mock) {
      return of(mockReponse);
    } else {
      let url: string = 'http://gym-budy-server/profile';
      return this.httpClient.post(url, profile);
    }
  }

  updateProfile(profile: IProfile): Observable<any> {
    console.log('create profile is running');
    if (mock) {
      return of(mockReponse);
    } else {
      let url: string = `${config.ServerUrl}/editBio`;
      return this.httpClient.put(url, profile);
    }
  }

  getFriendRequests(): Observable<string[]> {
    if (mock) {
      return of(mockFriendRequests);
    } else {
      let url: string = `${config.ServerUrl}/pendingFriendRequest?userName=${this.userProfile$.getValue().userName}`;
      return this.httpClient.get<string[]>(url);
    }
  }

  acceptFriendRequest(username: string) {
    if (mock) {
      return of(mockFriendRequests);
    } else {
      let url: string = `${config.ServerUrl}/acceptFriend`;
      let tmp: any = {
        userName: this.userProfile$.getValue().userName,
        friendUserName: username
      }
      return this.httpClient.post(url, tmp);
    }
  }

  rejectFriendRequest(username: string) {
    if (mock) {
      return of(mockFriendRequests);
    } else {
      let url: string = `${config.ServerUrl}/rejectFriend`;
      let tmp: any = {
        userName: this.userProfile$.getValue().userName,
        friendUserName: username
      }
      return this.httpClient.post(url, tmp);
    }
  }
}

let mockFriendRequests: any[] = [
  "user1",
  "user2",
  "user3"
]

let mockFriendUserProfile = {
  name: 'James Person',
  userName: 'jperson',
  age: 19,
  height: 0,
  weight: 180,
  bmi: 0,
  bio: 'some person i guess',
  status: 1
}

let mockPrivateUserProfile = {
  name: 'Purdue Pete',
  userName: 'pete',
  age: 19,
  height: 0,
  weight: 180,
  bmi: 0,
  bio: 'the real purdue pete',
  status: 0
}

let mockUserProfile = {
  name: 'Mickey',
  userName: 'Mickey123',
  age: 0,
  height: 0,
  weight: 0,
  bmi: 0,
  bio: 'My name is Mickey Mouse',
  status: 0
}

let mockReponse: any = {
  status: 0,
  messsage: "success"
}
