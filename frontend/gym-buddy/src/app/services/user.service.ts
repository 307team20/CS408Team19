import { Injectable } from '@angular/core';
import { IProfile, IUser, IFriendRequest } from '../lib/model';
import { IWorkout, IJournalEntry, IPost } from '../lib/model';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of, interval } from 'rxjs';
import { ProfileService } from './profile.service';
import { config } from '../lib/config';

let mock: boolean = false;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  workouts$: BehaviorSubject<IWorkout[]> = new BehaviorSubject<IWorkout[]>([]);
  journalEntries$: BehaviorSubject<IJournalEntry[]> = new BehaviorSubject<IJournalEntry[]>([]);
  postsFriends$: BehaviorSubject<IPost[]> = new BehaviorSubject<IPost[]>([]);
  postsUsers$: BehaviorSubject<IPost[]> = new BehaviorSubject<IPost[]>([]);

  constructor(private httpClient: HttpClient,
              private profileService: ProfileService) { }

  sendFriendRequest(friendUserName) {
    if (mock) {
    } else {
      let userName = this.profileService.userProfile$.getValue().userName;
      let url: string = `${config.ServerUrl}/addFriend`;
      return this.httpClient.post(url, {
        userName: userName,
        friendUserName: friendUserName
      });
    }
  }

  checkIfFriend(friendUserName) {
    console.log("friendUserName: " + friendUserName);
    // GET '/isFriend'
    if (mock) {
      return of(mockIsFriend);
    } else {
      let userName = this.profileService.userProfile$.getValue().userName;
      let url: string = `${config.ServerUrl}/isFriend?userName=${userName}&friendUserName=${friendUserName}`;
      return this.httpClient.get(url);
    }
  }

  updateWorkouts(userName) {
    if (mock) {
      of(mockWorkouts).subscribe(
        workouts => {
          this.workouts$.next(workouts);
        }, err => { }
      );
    } else {
      let url: string = `${config.ServerUrl}/viewAllWorkouts?userName=${userName}&callType=private`;
      this.httpClient.get<IWorkout[]>(url).subscribe(
        workouts => {
          this.workouts$.next(workouts);
        },
        err => {
        }
      );
    }

  }

  updateJournalEntries(userName) {
    if (mock) {
      of(mockJournalEntries).subscribe(
        journalEntries => {
          this.journalEntries$.next(journalEntries);
        },
        err => { }
      );
    } else {
      let url: string = `${config.ServerUrl}/viewAllDiets?userName=${userName}&callType=private`;
      this.httpClient.get<IJournalEntry[]>(url).subscribe(
        journalEntries => {
          this.journalEntries$.next(journalEntries);
        },
        err => {
        }
      );
    }
  }

  updatePosts(userName, friendsPost: boolean) {
    if (friendsPost) {
      if (mock) {
      } else {
        let url: string = `${config.ServerUrl}/viewFriendsPosts?userName=${userName}`;
        this.httpClient.get<IPost[]>(url).subscribe(
          posts => {
            this.postsFriends$.next(posts);
          }, err => {});
      }
    } else {
      if (mock) {
      } else {
        let url: string = `${config.ServerUrl}/viewAllPosts?userName=${userName}&callType=private`;
        this.httpClient.get<IPost[]>(url).subscribe(
          posts => {
            this.postsUsers$.next(posts);
          }, err => {});
      }
    }
  }



}

let mockIsFriend: any = {
  status: 1, // 1 === friend
  message: "success"
}

let mockJournalEntries: IJournalEntry[] = [
  {
    dName: 'Entry 1',
    date: '12/10/2017',
    email: "test@email",
    entry: 'Body 1',
    status: 0,
  },
  {
    dName: 'Entry 2',
    date: '12/10/2017',
    email: "test@email",
    entry: 'Body 2',
    status: 0,
  },
]
let mockWorkouts;
let mockFriendsPosts;
let mockUsersPosts;
