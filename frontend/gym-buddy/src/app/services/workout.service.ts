import { Injectable } from '@angular/core';
import { IWorkout, IJournalEntry, IPost } from '../lib/model';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ProfileService } from './profile.service';
import { config } from '../lib/config';

let mock: boolean = false;

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  workouts$: BehaviorSubject<IWorkout[]> = new BehaviorSubject<IWorkout[]>([]);
  journalEntries$: BehaviorSubject<IJournalEntry[]> = new BehaviorSubject<IJournalEntry[]>([]);
  postsFriends$: BehaviorSubject<IPost[]> = new BehaviorSubject<IPost[]>([]);
  postsUsers$: BehaviorSubject<IPost[]> = new BehaviorSubject<IPost[]>([]);

  constructor(private httpClient: HttpClient, private profileService: ProfileService) { }

  updateWorkouts() {
    if (mock) {
      /*
      of(mockWorkouts).subscribe(
        workouts => {
          this.workouts$.next(workouts);
        },
        err => {
        }
      );
      */
    } else {
      let url: string = `${config.ServerUrl}/viewAllWorkouts?userName=${this.profileService.userProfile$.getValue().userName}&callType=private`;
      this.httpClient.get<IWorkout[]>(url).subscribe(
        workouts => {
          this.workouts$.next(workouts);
        },
        err => {
        }
      );
    }
  }

  updateJournalEntries() {
    if (mock) {
      of(mockJournalEntries).subscribe(
        journalEntries => {
          this.journalEntries$.next(journalEntries);
        },
        err => { }
      );
    } else {
      let url: string = `${config.ServerUrl}/viewAllDiets?userName=${this.profileService.userProfile$.getValue().userName}&callType=private`;
      this.httpClient.get<IJournalEntry[]>(url).subscribe(
        journalEntries => {
          this.journalEntries$.next(journalEntries);
        },
        err => {
        }
      );
    }
  }

  updatePosts(friendsPost: boolean) {

    if (friendsPost) {
      if (mock) {
        /*
        of(mockFriendsPosts).subscribe(
          posts => {
            this.postsFriends$.next(posts);
          },
          err => {
          }
        );
        */
      } else {
        let url: string = `${config.ServerUrl}/viewFriendsPosts?userName=${this.profileService.userProfile$.getValue().userName}`;
        this.httpClient.get<IPost[]>(url).subscribe(
          posts => {
            this.postsFriends$.next(posts);
          },
          err => {
          }
        );
      }
    } else {
      if (mock) {
        /*
        of(mockUsersPosts).subscribe(
          posts => {
            this.postsUsers$.next(posts);
          },
          err => {
          }
        );
        */
      } else {
        /*
        let url: string = `${config.ServerUrl}/viewAllPosts?userName=${this.profileService.userProfile$.getValue().userName}&callType=private`;
        this.httpClient.get<IPost[]>(url).subscribe(
          posts => {
            this.postsUsers$.next(posts);
          },
          err => {
          }
        );
        */
        let url: string = `${config.ServerUrl}/viewFriendsPosts?userName=${this.profileService.userProfile$.getValue().userName}`;
        this.httpClient.get<IPost[]>(url).subscribe(
          posts => {
            this.postsUsers$.next(posts);
          },
          err => {
          }
        );
      }
    }
  }
}

/*
let mockFriendsPosts: IPost[] = [
  {
    username: 'user1',
    text: 'This is a post 1',
    workouts: [{ title: 'Workout 1', date: '12/10/2018' }, { title: 'Workout 2', date: '12/10/2018' }],
    journals: [{ title: 'Entry 1', date: '12/10/2018', body: 'Body 1' }, { title: 'Entry 2', date: '12/10/2018', body: 'Body 2' }]
  },
  {
    username: 'user2',
    text: 'This is a post 2',
    workouts: [{ title: 'Workout 8', date: '12/10/2018' }, { title: 'Workout 9', date: '12/10/2018' }],
    journals: [{ title: 'Entry 8', date: '12/10/2018', body: 'Body 8' }, { title: 'Entry 9', date: '12/10/2018', body: 'Body 9' }]
  },
]


let mockUsersPosts: IPost[] = [
  {
    username: 'mine1',
    text: 'This is a post 1',
    workouts: [{ title: 'Workout 1', date: '12/10/2018' }, { title: 'Workout 2', date: '12/10/2018' }],
    journals: [{ title: 'Entry 1', date: '12/10/2018', body: 'Body 1' }, { title: 'Entry 2', date: '12/10/2018', body: 'Body 2' }]
  },
  {
    username: 'mine2',
    text: 'This is a post 2',
    workouts: [{ title: 'Workout 8', date: '12/10/2018' }, { title: 'Workout 9', date: '12/10/2018' }],
    journals: [{ title: 'Entry 8', date: '12/10/2018', body: 'Body 8' }, { title: 'Entry 9', date: '12/10/2018', body: 'Body 9' }]
  },
  {
    username: 'mine3',
    text: 'This is a post 2',
    workouts: [{ title: 'Workout 8', date: '12/10/2018' }, { title: 'Workout 9', date: '12/10/2018' }],
    journals: [{ title: 'Entry 8', date: '12/10/2018', body: 'Body 8' }, { title: 'Entry 9', date: '12/10/2018', body: 'Body 9' }]
  },
  {
    username: 'mine4',
    text: 'This is a post 2',
    workouts: [{ title: 'Workout 8', date: '12/10/2018' }, { title: 'Workout 9', date: '12/10/2018' }],
    journals: [{ title: 'Entry 8', date: '12/10/2018', body: 'Body 8' }, { title: 'Entry 9', date: '12/10/2018', body: 'Body 9' }]
  },
]
*/

/*
let mockWorkouts: IWorkout[] = [
  {
    title: 'Workout 1',
    date: '12/10/2017',
  },
  {
    title: 'Workout 2',
    date: '12/11/2017',
  },
  {
    title: 'Workout 3',
    date: '12/10/2017',
  },
  {
    title: 'Workout 4',
    date: '12/11/2017',
  },
  {
    title: 'Workout 5',
    date: '12/10/2017',
  },
  {
    title: 'Workout 6',
    date: '12/11/2017',
  },
  {
    title: 'Workout 7',
    date: '12/10/2017',
  },
  {
    title: 'Workout 8',
    date: '12/11/2017',
  },
]
*/

let mockJournalEntries: IJournalEntry[] = [
  {
    dName: 'Entry 1',
    date: '12/10/2017',
    email: "test@email",
    entry: 'Body 1',
    status: 0,
  },
  {
    dName: 'Entry 2',
    date: '12/10/2017',
    email: "test@email",
    entry: 'Body 2',
    status: 0,
  },
]
