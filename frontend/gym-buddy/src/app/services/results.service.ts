import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of, interval } from 'rxjs';
import { config } from '../lib/config';
import { ProfileService } from './profile.service';

let mock: boolean = false;

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  constructor(private httpClient: HttpClient,
              private profileService: ProfileService) { }

  getUsersResults(searchString): Observable<any> {
    console.log('get user results is running');
    console.log('searchString: ' + searchString);
    if (mock) {
      return of(mockResults);
    } else {
      let url: string = `${config.ServerUrl}/searchByUser`;
      console.log("running post to: " + url);
      let res = this.httpClient.post(url, {
        searchQuery: searchString,
        userName: this.profileService.userProfile$.getValue().userName
      });
      console.log("returned from client");
      console.log(res);
      return res;
    }
  }

  getTagsResults(searchString): Observable<any> {
    console.log('get tag results is running');
    console.log('searchString: ' + searchString);
    if (mock) {
      return of(mockResults);
    } else {
      let url: string = `${config.ServerUrl}/searchByTag`;
      console.log("running post to: " + url);
      let searchTags = searchString.split(" ");
      let res = this.httpClient.post(url, {
        tag: searchTags,
        userName: this.profileService.userProfile$.getValue().userName
      });
      console.log("returned from client");
      console.log(res);
      return res;
    }
  }
}

let mockResults = [
  {
    username: "jperson",
    email: "jperson@purdue.edu",
    name: "James Person",
  },
  {
    username: "gfrace",
    email: "gfrance@purdue.edu",
    name: "Guy LeFrancais",
  },
  {
    username: "pete",
    email: "pete@purdue.edu",
    name: "Purdue Pete",
  }
]

let mockResponse: any = {
  status: 0,
  message: "success"
}
