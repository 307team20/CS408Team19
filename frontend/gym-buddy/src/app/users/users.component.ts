import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../services/user.service';
import { IProfile, IUser, IFriendRequest } from '../lib/model';
import { IWorkout, IJournalEntry, IPost } from '../lib/model';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from '../services/profile.service';
import { WorkoutService } from '../services/workout.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  profileStatus: string = 'private';

  workoutAllFlag: boolean = false;
  workoutButtonText = "Show All";
  workouts: IWorkout[] = [];
  workoutsFew: IWorkout[] = [];
  workoutsAll: IWorkout[] = [];
  workoutsAllTmp: IWorkout[] = [];

  journalAllFlag: boolean = false;
  journalButtonText = "Show All";
  journalEntries: IJournalEntry[] = [];
  journalEntriesFew: IJournalEntry[] = [];
  journalEntriesAll: IJournalEntry[] = [];

  postAllFlag: boolean = false;
  postButtonText = "Show All";
  posts: IPost[] = [];
  postsFew: IPost[] = [];
  postsAll: IPost[] = [];

  @ViewChild('f') form;

  profileFriend: IProfile = {
    name: '',
    userName: '',
    age: 0,
    height: 0,
    weight: 0,
    bmi: 0,
    bio: '',
    status: 0
  };

  user$: Object;
  canViewBio: boolean = false;
  isFriend: boolean = false;
  friendReqSent: boolean = false;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private profileService: ProfileService,
              private workoutService: WorkoutService) {
    this.route.params.subscribe( params => this.user$ = params.username);
  }

  sendFriendRequest() {
    let friendUserName = this.user$;
    console.log("sendFriendRequest clicked");
    console.log("friendUserName: "+friendUserName);
    this.userService.sendFriendRequest(friendUserName).subscribe(res => {
      console.log("server response received from POSTing friend request");
      console.log(res);
      if (res['message'] === 'Friend request has been sent') {
        console.log("all good, can disable button");
        this.friendReqSent = true;
      } else if (res === 'Request Exists!') {
        console.log("had already sent, can disable button");
        this.friendReqSent = true;
      }
    });
  }

  ngOnInit() {
    let userName = this.profileService.userProfile$.getValue().userName;
    let otherUserName = this.user$;
    this.userService.checkIfFriend(otherUserName).subscribe(res => {
      console.log("isFriend res");
      console.log(res);
      if (res.status) {
        this.isFriend = true;
      } else {
        this.isFriend = false;
      }

      this.profileService.getOtherProfile(otherUserName).subscribe(data => {

        console.log('getOtherProfile res:');
        console.log(data);
        this.profileFriend = data;
        let status = this.profileFriend.status;
        if (status == 0) {
          this.profileStatus = 'Private';
          if (this.isFriend) {
            this.canViewBio = true;
          } else {
            this.canViewBio = false;
          }
        } else if (status == 1) {
          this.profileStatus = 'Friends Only';
          if (this.isFriend) {
            this.canViewBio = true;
          } else {
            this.canViewBio = false;
          }
        } else {
          this.profileStatus = 'Public';
          this.canViewBio = true;
        }

        if (this.profileFriend.height != 0 && this.profileFriend.height != null) {
          this.profileFriend.bmi = 723 * (this.profileFriend.weight / Math.pow(this.profileFriend.height, 2));
        } else {
          this.profileFriend.bmi = 0;
        }

        this.userService.updateWorkouts(otherUserName);
        this.userService.updateJournalEntries(otherUserName);
        this.userService.updatePosts(otherUserName, false);

        // 1
        this.userService.workouts$.subscribe(workouts => {
          console.log("workouts");
          console.log(workouts);
          if (!workouts) return;

          this.workouts = workouts;
          if (this.workouts.length < 6) {
            this.workoutsFew = this.workouts.slice(0, this.workouts.length + 1);
          } else {
            this.workoutsFew = this.workouts.slice(0, 6);
          }
          this.workouts = this.workoutsFew;
        }, err => {});

        // 2
        this.userService.journalEntries$.subscribe(journalEntries => {
          console.log("journalEntries");
          console.log(journalEntries);
          if (!journalEntries) return;

          this.journalEntriesAll = journalEntries;
          this.journalEntries = this.journalEntriesAll;
          if (this.journalEntries.length < 6) {
            this.journalEntriesFew = this.journalEntries.slice(0, this.journalEntries.length + 1);
          } else {
            this.journalEntriesFew = this.journalEntries.slice(0, 6);
          }
          this.journalEntries = this.journalEntriesFew;
        }, err => {});

        // 3
        this.userService.postsUsers$.subscribe(posts => {
          console.log("posts");
          console.log(posts);
          if (!posts) return;

          this.postsAll = posts;

          if (posts.length < 3) {
            this.postsFew = posts.slice(0, posts.length + 1);
          } else {
            this.postsFew = posts.slice(0, 3);
          }
          this.posts = this.postsFew;
        }, err => {});

      });

    });
  }


  workoutShow() {
    if (this.workoutAllFlag) {
      this.workouts = this.workoutsFew;
      this.workoutButtonText = "Show All";
    } else {
      this.workouts = this.workoutsAll;
      this.workoutButtonText = "Show Less";
    }
    this.workoutAllFlag = !this.workoutAllFlag;
  }

  journalShow() {
    if (this.journalAllFlag) {
      this.journalEntries = this.journalEntriesFew;
      this.journalButtonText = "Show All";
    } else {
      this.journalEntries = this.journalEntriesAll;
      this.journalButtonText = "Show Less";
    }
    this.journalAllFlag = !this.journalAllFlag;
  }

  postShow() {
    if (this.postAllFlag) {
      this.posts = this.postsFew;
      this.postButtonText = "Show All";
    } else {
      this.posts = this.postsAll;
      this.postButtonText = "Show Less";
    }
    this.postAllFlag = !this.postAllFlag;
  }

}
