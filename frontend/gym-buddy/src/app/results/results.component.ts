import { Component, OnInit } from '@angular/core';
import { ResultsService } from '../services/results.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})

export class ResultsComponent implements OnInit {
  people = [];
  tags = [];
  searchType = '';

  constructor(private resultsService: ResultsService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    console.log('ngOnInit (results component): running');
    this.route.queryParams.subscribe(params => {
      let searchType = params['type'];
      this.searchType = searchType;
      let searchString = params['search'];

      if (searchType === 'users') {
        this.resultsService
          .getUsersResults(searchString)
          .subscribe(results => {
            this.tags = [];
            console.log("user results");
            console.log(results);
            if (results.message === "User not found") {
              console.log("returning early");
              this.people = [];
              return;
            }
            this.people = results;
          });
      }
      if (searchType === 'tags') {
        this.resultsService
          .getTagsResults(searchString)
          .subscribe(results => {
            this.people = [];
            console.log("tag results");
            console.log(results);
            if (results.message === "Tags not found") {
              console.log("returning early");
              this.tags = [];
              return;
            }
            this.tags = results;
          });
      }


    });
  }

}
