import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from './services/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  isLoggedIn: boolean = false;

  constructor(private accountService: AccountService, private router: Router) { }

  ngOnInit() {
    this.accountService.loggedIn$.subscribe(
      val => {
        this.isLoggedIn = val;
        if (!val) {
          this.router.navigate(['/login']);
        }
      }
    );
  }

}
