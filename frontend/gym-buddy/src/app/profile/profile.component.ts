import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { IProfile } from '../lib/model';
import { ProfileService } from '../services/profile.service';
import { WorkoutService } from '../services/workout.service';
import { IWorkout, IJournalEntry, IPost } from '../lib/model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  creationStatus: string = 'update error';
  profileStatus: string = 'private';

  workoutAllFlag: boolean = false;
  workoutButtonText = "Show All";
  workouts: IWorkout[] = [];
  workoutsFew: IWorkout[] = [];
  workoutsAll: IWorkout[] = [];

  journalAllFlag: boolean = false;
  journalButtonText = "Show All";
  journalEntries: IJournalEntry[] = [];
  journalEntriesFew: IJournalEntry[] = [];
  journalEntriesAll: IJournalEntry[] = [];

  postAllFlag: boolean = false;
  postButtonText = "Show All";
  posts: IPost[] = [];
  postsFew: IPost[] = [];
  postsAll: IPost[] = [];

  @ViewChild('f') form;

  profileMaster: IProfile = {
    name: '',
    userName: '',
    age: 0,
    height: 0,
    weight: 0,
    bmi: 0,
    bio: '',
    status: 0
  };
  profile: IProfile;

  isOpen: boolean = false;

  updateFailed: string = null;

  constructor(private profileService: ProfileService, private workoutService: WorkoutService) {
    //this.profile = JSON.parse(JSON.stringify(this.profileMaster));
  }

  ngOnInit() {
    this.profileMaster = this.profileService.userProfile$.getValue();
    if (this.profileMaster.status == 0) {
      this.profileStatus = 'Private';
    } else if (this.profileMaster.status == 1) {
      this.profileStatus = 'Friends Only';
    } else {
      this.profileStatus = 'Public';
    }
    if (this.profileMaster.height != 0 && this.profileMaster.height != null) {
      this.profileMaster.bmi = 723 * (this.profileMaster.weight / Math.pow(this.profileMaster.height, 2));
    } else {
      this.profileMaster.bmi = 0;
    }
    this.profile = JSON.parse(JSON.stringify(this.profileMaster)); // make copy of master for editing
    this.workoutService.updateWorkouts();
    this.workoutService.updateJournalEntries();
    this.workoutService.updatePosts(false);
    this.workoutService.workouts$.subscribe(
      workouts => {
        this.workoutsAll = workouts;
        if (workouts.length < 6) {
          this.workoutsFew = workouts.slice(0, workouts.length + 1);
        } else {
          this.workoutsFew = workouts.slice(0, 6);
        }
        this.workouts = this.workoutsAll;
      },
      err => {
      }
    );
    this.workoutService.journalEntries$.subscribe(
      journalEntries => {
        this.journalEntriesAll = journalEntries;
        if (journalEntries.length < 6) {
          this.journalEntriesFew = journalEntries.slice(0, journalEntries.length + 1);
        } else {
          this.journalEntriesFew = journalEntries.slice(0, 6);
        }
        this.journalEntries = this.journalEntriesAll;
      },
      err => {
      }
    );
    this.workoutService.postsUsers$.subscribe(
      posts => {
        for (let post of posts) {
          post.workout = [];
          post.diet = [];
        }
        this.postsAll = posts;
        if (posts.length < 3) {
          this.postsFew = posts.slice(0, posts.length + 1);
        } else {
          this.postsFew = posts.slice(0, 3);
        }
        this.posts = this.postsFew;
      },
      err => {
      }
    );
  }

  workoutShow() {
    if (this.workoutAllFlag) {
      //this.workouts = this.workoutsFew;
      this.workouts = this.workoutsAll;
      this.workoutButtonText = "Show All";
    } else {
      //this.workouts = this.workoutsAll;
      this.workouts = [];
      this.workoutButtonText = "Show Less";
    }
    this.workoutAllFlag = !this.workoutAllFlag;
  }

  journalShow() {
    if (this.journalAllFlag) {
      //this.journalEntries = this.journalEntriesFew;
      this.journalEntries = this.journalEntriesAll;
      this.journalButtonText = "Show All";
    } else {
      this.journalEntries = this.journalEntriesFew;
      //this.journalEntries = this.journalEntriesAll;
      this.journalButtonText = "Show Less";
    }
    this.journalAllFlag = !this.journalAllFlag;
  }

  postShow() {
    if (this.postAllFlag) {
      this.posts = this.postsFew;
      this.postButtonText = "Show All";
    } else {
      this.posts = this.postsAll;
      this.postButtonText = "Show Less";
    }
    this.postAllFlag = !this.postAllFlag;
  }

  onUpdate() {
    this.updateFailed = null;
    if (this.profile.age < 15 || this.profile.age > 120) {
      this.updateFailed = 'Please enter an age between 15 and 120.'
    }
    if (this.profile.height < 12 || this.profile.height > 120) {
      if (this.updateFailed) {
        this.updateFailed += '<br/>Please enter a height between 12 and 120.'
      } else {
        this.updateFailed = 'Please enter a height between 12 and 120.'
      }
    }
    if (this.profile.weight < 50 || this.profile.age > 1000) {
      if (this.updateFailed) {
        this.updateFailed += '<br/>Please enter a weight between 50 and 999.'
      } else {
        this.updateFailed = 'Please enter a weight between 50 and 999.'
      }
    }
    if (!this.updateFailed) {
      this.isOpen = false;
      this.profileService.updateProfile(this.profile).subscribe(
        result => {
          this.profileMaster = JSON.parse(JSON.stringify(this.profile));
          if (this.profileMaster.status == 0) {
            this.profileStatus = 'Private';
          } else if (this.profileMaster.status == 1) {
            this.profileStatus = 'Friends Only';
          } else {
            this.profileStatus = 'Public';
          }
          if (this.profileMaster.height != 0) {
            this.profileMaster.bmi = 723 * (this.profileMaster.weight / Math.pow(this.profileMaster.height, 2));
          } else {
            this.profileMaster.bmi = 0;
          }
        },
        err => {
        }
      );
    } else {
      this.profileService.updateProfile(this.profile).subscribe(
        result => {
          this.profileMaster = JSON.parse(JSON.stringify(this.profile));
          if (this.profileMaster.status == 0) {
            this.profileStatus = 'Private';
          } else if (this.profileMaster.status == 1) {
            this.profileStatus = 'Friends Only';
          } else {
            this.profileStatus = 'Public';
          }
          if (this.profileMaster.height != 0) {
            this.profileMaster.bmi = 723 * (this.profileMaster.weight / Math.pow(this.profileMaster.height, 2));
          } else {
            this.profileMaster.bmi = 0;
          }
        },
        err => {
        }
      );
    }
  }

  onCancel() {
    this.isOpen = false;
    this.updateFailed = null;
  }

  openModal() {
    this.onCancel();
    this.isOpen = true;
  }

}


