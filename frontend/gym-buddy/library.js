var ajaxURL = "http://18.191.214.116:5000";
var update = false;
function populateLists() {
	var xhttp = new XMLHttpRequest();
	var xhttp2 = new XMLHttpRequest();
	var user = getUserName();
	var workoutStatus;
	var foodStatus;
	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	       var workouts = JSON.parse(xhttp.responseText);
	       if (workouts.length < 1) {
	       	document.getElementById("noWorkoutsSpan").style.display = 'block';
	       } else {
	       	for (var i = 0; i < workouts.length; i++) {
				if (workouts[i]["status"] == 0) workoutStatus = "private";
				else if (workouts[i]["status"] == 1) workoutStatus = "friend";
				else workoutStatus = "public";
				var row = document.getElementById("plannedWorkouts").insertRow(i);
				row.id = workouts[i]["date"];
				row.title = workoutStatus;
				//Bug, only adds onclick to the last index
				row.onclick = function() { showEditDeleteModal(i - 1, 'plannedWorkouts', 'workout') };
				/*
				(function(index) {
					row.onclick = function() {showEditDeleteModal(index, 'plannedWorkouts','workout')};
				})(i);*/
				row.innerHTML = workouts[i]["wName"];
				row.style.cursor = "pointer";
	       	}
	       }
	    }
	};
	xhttp.open("GET", ajaxURL + "/viewAllWorkouts?userName=" + user + "&callType=private", true);
	xhttp.send();
	//Food Journals
	var start = 0;
	xhttp2.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	       var foodJournals = JSON.parse(xhttp2.responseText);
	       if (foodJournals.length < 1) {
	       	document.getElementById("noFoodSpan").style.display = 'block';
	       } else {
	       	for (var i = start; i < foodJournals.length; i++) {
				if (foodJournals[i]["status"] == 0) foodStatus = "private";
				else if (foodJournals[i]["status"] == 1) foodStatus = "friend";
				else foodStatus = "public";
				var row = document.getElementById("foodJournals").insertRow(i);
				row.id = foodJournals[i]["date"];
				row.title = foodStatus;
				(function(index) {
					row.onclick = function() {showEditDeleteModal(index, 'foodJournals','food')};
				})(i);
				row.innerHTML = foodJournals[i]["dName"];
				row.style.cursor = "pointer";
	       	}
	       }
	    }
	};
	xhttp2.open("GET", ajaxURL + "/viewAllDiets?userName=" + user + "&callType=private", true);
	xhttp2.send();
}
function showHide(toShow, toHide) {
	document.getElementById(toHide).style.display = 'none';
	document.getElementById(toShow).style.display = 'block';
}
function newWorkout() {
	document.getElementById('workoutName').value = "";
	var date = new Date();
	document.getElementById('workoutDate').valueAsDate = date;
	document.getElementById("workoutInfo").innerHTML = "Your workout is currently empty";
	document.getElementById("showTags").innerHTML = "";
	document.getElementById("showTags").style.display = 'none';
	showHide('workoutDisplay', 'workoutDashboard');
}
function showEditDeleteModal(index, table, type) {
	var date = document.getElementById(table).rows[index].id;
	if (type == "workout") {
		document.getElementById('toEditDeleteName').innerHTML = document.getElementById(table).rows[index].innerHTML;
		document.getElementById('loadWorkoutButton').onclick = function() { update = true; loadWorkout(index, table, date); }
		document.getElementById('addToPostButton').onclick = function() { addWorkoutToPost(index, table, date); }
		document.getElementById('deleteWorkoutButton').onclick = function() { deleteWorkout(index, table, date); }
		document.getElementById('workoutEditDiv').style.display = 'block';
		document.getElementById('foodEditDiv').style.display = 'none';
	} else if (type == "food") {
		document.getElementById('toEditDeleteFoodName').innerHTML = document.getElementById(table).rows[index].innerHTML;
		document.getElementById('updateJournal').onclick = function() { updateFoodJournal(index, table, date); }
		document.getElementById('addFoodToPostButton').onclick = function() { addFoodJournalToPost(index, table, date); }
		document.getElementById('deleteFoodJournalButton').onclick = function() { deleteFoodJournal(index, table, date); }
		document.getElementById('toEditDeleteFoodDate').innerHTML = document.getElementById(table).rows[index].id;

		//Ajax call to get entry
		var xhttp2 = new XMLHttpRequest();
		var user = getUserName();
		xhttp2.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	var foodJournals = JSON.parse(xhttp2.responseText);
		    	var newIndex = index;
		       	for (var i = 0; i < foodJournals.length; i++) {
	       			if (document.getElementById(table).rows[newIndex].innerHTML == foodJournals[i]["dName"]) {		       		
						document.getElementById('displayJournal').innerHTML = foodJournals[i]["entry"];					
					}		       	
		       	}
		    }
		};
		xhttp2.open("GET", ajaxURL + "/viewAllDiets?userName=" + user + "&callType=private", true);
		xhttp2.send();

		document.getElementById('foodEditDiv').style.display = 'block';
		document.getElementById('workoutEditDiv').style.display = 'none';
	}
	document.getElementById('editDeleteModal').style.display = 'block';
}
function hideEditDeleteModal() {
	document.getElementById('editDeleteModal').style.display = 'none';
}
//Consider adding date as well since it is an id
function loadWorkout(index, table, date) {
	//Another weird intereaction, everytime an xhttp call is made variables named index are set to undefined
	var newIndex = index;
	console.log("load" + index);
	console.log("loading: " + document.getElementById(table).rows[index].innerHTML);
	showHide('workoutDisplay', 'workoutDashboard');
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	       var workouts = JSON.parse(xhttp.responseText);
	       	for (var i = 0; i < workouts.length; i++) {
	       		if (document.getElementById(table).rows[newIndex].innerHTML == workouts[i]["wName"]) {
					document.getElementById('workoutName').value = document.getElementById(table).rows[newIndex].innerHTML;
					document.getElementById('workoutDate').value = workouts[i]["date"];
					console.log(workouts[i]["tags"]);
					for (var j = 0; j < workouts[i]["tags"].length; j++) {
						document.getElementById('showTags').innerHTML += workouts[i]["tags"][j] + "<br>";
						document.getElementById('showTags').style.display = 'block';
					}
					var exercises = workouts[i]["exercises"];
					var length = 0;
					if (Object.keys(exercises).length !== 0) {
						document.getElementById("workoutInfo").innerHTML = "";
						for (e in exercises) {
							document.getElementById("workoutInfo").innerHTML += e + ": " + exercises[e] + "<br>";
						}
					}
					document.getElementById("spotifyURL").value = workouts[i]["url"];
					hideEditDeleteModal();
					hidePostModal();	       			
	       		}
	       	}
	    }
	};
	var user = getUserName();
	xhttp.open("GET", ajaxURL + "/viewAllWorkouts?userName=" + user + "&callType=private", true);
	xhttp.send();
}
function deleteWorkout(index, table, date) {
	var SQLDate = toSQLDate(date);
	var user = getUserName();
	var package = JSON.stringify({
		"date": SQLDate, 
		"wName": document.getElementById(table).rows[index].innerHTML, 
		"userName": user});
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() { 
	    if (this.readyState == 4 && this.status == 200) {
			//window.location.replace(url + "/workout-create");
			//window.location = url  + "/profile"; 
			location.reload();
		}
	}
	xhttp.open("DELETE", ajaxURL + "/deleteWorkout", true);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhttp.send(package);
	hideEditDeleteModal();
}
function updateFoodJournal(date) {
	if (confirm("Are you sure you want to edit?")) {
		var package = getFoodInfo();

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() { 
		    if (this.readyState == 4 && this.status == 200) {
				location.reload();
			}
		}
		xhttp.open("PUT", ajaxURL + "/editDiet", true);
		xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xhttp.send(package);

		//Reset workout info
		document.getElementById('foodName').value = "";
		document.getElementById('foodDate').valueAsDate = new Date();
		document.getElementById('foodJournalText').value = "";
	}
}
function deleteFoodJournal(index, table, date) {
	var SQLDate = toSQLDate(date);
	var user = getUserName();
	var package = JSON.stringify({
		"date": SQLDate, 
		"dName": document.getElementById(table).rows[index].innerHTML, 
		"userName": user});

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() { 
	    if (this.readyState == 4 && this.status == 200) {
			location.reload();
		}
	}
	xhttp.open("DELETE", ajaxURL + "/deleteDiet", true);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhttp.send(package);
	hideEditDeleteModal();	
}
function addPost() {
	populateDropdown("workoutDropdown");
	populateDropdown("foodDropdown");	
	showPostModal();
}
function postPrivacySelection() {
	var privacy = document.getElementById("postPrivacy").value;
	console.log(privacy);
	removePrivacyOptions(document.getElementById("workoutDropdown"), privacy);
	removePrivacyOptions(document.getElementById("foodDropdown"), privacy);
	document.getElementById("postDisable").classList.remove("disabled");
}
function addWorkoutToPost(index, table, date) {
	if (document.getElementById(table).rows[index].title == "private") {
		alert("Private Workouts Cannot Be Posted");
		return;
	}
	hideEditDeleteModal();
	document.getElementById("postWorkoutButton").innerHTML = document.getElementById(table).rows[index].innerHTML;
	document.getElementById("postWorkoutButton").onclick = function() { loadWorkout(index, table); }
	document.getElementById("foodDropdown").style.visibilty = 'visible';
	document.getElementById("postWorkoutDate").value = date;
	document.getElementById("postPrivacy").value = document.getElementById(table).rows[index].title;
	document.getElementById("postPrivacy").disabled = true;
	document.getElementById("postDisable").classList.remove("disabled");
	populateDropdown("foodDropdown");
	removePrivacyOptions(document.getElementById("foodDropdown"), document.getElementById("postPrivacy").value);
	showPostModal();
}
function addFoodJournalToPost(index, table, date) {
	if (document.getElementById(table).rows[index].title == "private") {
		alert("Private Food Journals Cannot Be Posted");
		return;
	}
	hideEditDeleteModal();
	document.getElementById("postFoodButton").innerHTML = document.getElementById(table).rows[index].innerHTML;
	document.getElementById("postFoodButton").onclick = function() { console.log("View journal?") }
	document.getElementById("workoutDropdown").style.visibilty = 'visible';
	document.getElementById("postFoodDate").value = date;
	document.getElementById("postPrivacy").value = document.getElementById(table).rows[index].title;
	document.getElementById("postPrivacy").disabled = true;
	document.getElementById("postDisable").classList.remove("disabled");
	populateDropdown("workoutDropdown");
	removePrivacyOptions(document.getElementById("workoutDropdown"), document.getElementById("postPrivacy").value);	
	showPostModal();
}
function submitPost() {
	//_email = request.json['email']
	user = getUserName();
	//Sets workout date to hidden input (Correct if post made from workout)
    var workoutDate = document.getElementById("postWorkoutDate").value;
    //Sets workout name to button name (Correct if post made from workout)
    var workoutName = document.getElementById('postWorkoutButton').innerHTML;
    //If the workout dropdown is visible, we choose the selected value as workout name
    if (document.getElementById('workoutDropdown').style.visibility == 'visible') {
    	//Secret key splits strings, [0] is name, [1] is date
    	workoutInfo = document.getElementById('workoutDropdown').value.split("~?.+_%^");
    	workoutName = workoutInfo[0];
    	workoutDate = workoutInfo[1];
    }
    //Otherwise, if the post is new and the button says Add workout then no workout is selected
    if (workoutName == "Add Workout") {
    	workoutName = "";
    	workoutDate = "";
    }

    //Same as workout, see above
    var foodDate = document.getElementById("postFoodDate").value;
    var foodName = document.getElementById('postFoodButton').innerHTML;
    if (document.getElementById('foodDropdown').style.visibility == 'visible') {
    	foodInfo = document.getElementById('foodDropdown').value.split("~?.+_%^");
    	foodName = foodInfo[0];
    	foodDate = foodInfo[1];
    }
    if (foodName == 'Add Food Journal') {
    	foodName = "";
    	foodDate = "";
    }

    //_entry
	var postEntry = document.getElementById('postText').value;
    //_pName
    var postName = document.getElementById('postName').value;
    //_pDate
    var postDate = toSQLDate(new Date());
    //_status
    var status = document.getElementById('postPrivacy').value;
    var postStatus = 2;
    if (status == "private") postStatus = 0;
    else if (status = "friend") postStatus = 1;
    else status = 2;
    //Bug, post date is set as food date
	var package = JSON.stringify({
		"userName": user, 
		"wDate": workoutDate, 
		"wName": workoutName, 
		"dDate": postDate, 
		"dName": foodName, 
		"entry": postEntry,
		"pName": postName,
		"pDate": postDate,
		"status": postStatus});

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() { 
	    if (this.readyState == 4 && this.status == 200) {
			location.reload();
		}
	}
	xhttp.open("PUT", ajaxURL + "/addPost", true);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhttp.send(package);

	removeOptions(document.getElementById('workoutDropdown'));
	removeOptions(document.getElementById('foodDropdown'));
	hidePostModal();
	//location.reload();
}
function showFoodModal() {
	document.getElementById("foodModal").style.display = 'block';
	document.getElementById("foodDate").valueAsDate = new Date();
}
function hideFoodModal() {
	document.getElementById("foodModal").style.display = 'none';
}
function submitFoodJournal() {
	if (confirm("Are you sure you want to submit?")) {
		var package = getFoodInfo();

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() { 
		    if (this.readyState == 4 && this.status == 200) {
				location.reload();
			}
		}
		xhttp.open("PUT", ajaxURL + "/addDiet", true);
		xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xhttp.send(package);
		//Reset workout info
		document.getElementById('foodName').value = "";
		document.getElementById('foodDate').valueAsDate = new Date();
		document.getElementById('foodJournalText').value = "";
	}	
}
function showRepModal(exercise, workoutType, tag) {
	document.getElementById('ChosenExercise').innerHTML = exercise;
	//Resets values to 0/""
	document.getElementById('reps').value = 0;
	document.getElementById('timeHours').value = 0;
	document.getElementById('timeMinutes').value = 0;
	document.getElementById('activityInput').value = "";
	if (workoutType == "reps") {
		document.getElementById("numReps").style.display = 'block';
		document.getElementById("reps").style.display = 'block';
		document.getElementById("timeSpent").style.display = 'none';
		document.getElementById("timeHours").style.display = 'none';
		document.getElementById("timeMinutes").style.display = 'none';
		document.getElementById("textHours").style.display = 'none';
		document.getElementById("textMinutes").style.display = 'none';
		document.getElementById("activity").style.display = 'none';
		document.getElementById("activityInput").style.display = 'none';
	} else if (workoutType == "time") {
		document.getElementById("numReps").style.display = 'none';
		document.getElementById("reps").style.display = 'none';
		document.getElementById("timeSpent").style.display = 'block';
		document.getElementById("timeHours").style.display = 'block';
		document.getElementById("timeMinutes").style.display = 'block';
		document.getElementById("textHours").style.display = 'block';
		document.getElementById("textMinutes").style.display = 'block';
		document.getElementById("activity").style.display = 'none';
		document.getElementById("activityInput").style.display = 'none';
	} else {
		document.getElementById("numReps").style.display = 'none';
		document.getElementById("reps").style.display = 'none';
		document.getElementById("timeSpent").style.display = 'none';
		document.getElementById("timeHours").style.display = 'none';
		document.getElementById("timeMinutes").style.display = 'none';
		document.getElementById("textHours").style.display = 'none';
		document.getElementById("textMinutes").style.display = 'none';
		document.getElementById("activity").style.display = 'block';
		document.getElementById("activityInput").style.display = 'block';			
	}
	document.getElementById('repModal').style.display = 'block';
	if (document.getElementById("showTags").innerHTML == "") {
		document.getElementById("showTags").innerHTML += "Tags:<br>" + tag;
		document.getElementById("showTags").style.display = 'block';
	} else {
		if (document.getElementById("showTags").innerHTML.includes(tag)) {
			console.log("already added");
		} else {
			document.getElementById("showTags").innerHTML += "<br>" + tag;
		}
	}
}
function hideRepModal() {
	document.getElementById('repModal').style.display = 'none';		
}
function showSpotifyModal() {
	document.getElementById('spotifyModal').style.display = 'block';		
}
function hideSpotifyModal() {
	document.getElementById('spotifyModal').style.display = 'none';		
}
function showPostModal() {
	document.getElementById('postModal').style.display = 'block';
}
function hidePostModal() {
	document.getElementById('postModal').style.display = 'none';
	document.getElementById('foodDropdown').style.visibility = 'hidden';
	document.getElementById('workoutDropdown').style.visibility = 'hidden';
	document.getElementById('postDisable').classList.add("disabled");
	removeOptions(document.getElementById('workoutDropdown'));
	removeOptions(document.getElementById('foodDropdown'));
}
function addToWorkoutInfo() {
	var reps = document.getElementById('reps').value;
	var hours = document.getElementById('timeHours').value;
	var minutes = document.getElementById('timeMinutes').value;
	var activity = document.getElementById('activityInput').value;
	if (document.getElementById('reps').style.display != 'none') {
		if (reps == 0) {
			alert("You forgot to add reps");
			return;
		}
		if (reps < 0) {
			alert("You cannot have negative reps");
			return;
		}
		//Upper bound for reps?
	}
	if (document.getElementById('timeHours').style.display != 'none') {
		//Bug, removing negative checks
		/*
		if (hours < 0) {
			alert("Invalid Time: Cannot have negative hours");
		}*/
		if (hours > 23) {
			alert("Invalid Time: Hours cannot exceed 23");
			return;
		}
		if (minutes < 0) {
			alert("Invalid Time: Cannot have negative minutes");
		}
		if (minutes > 59) {
			alert("Invalid Time: Minutes cannot exceed 59");
			return;
		}
	}
	if (document.getElementById('activity').style.display != 'none' && activity == "") {
		alert("You forgot to specify what the facility will be used for");
		return;
	}
	if (document.getElementById('workoutInfo').innerHTML == "Your workout is currently empty") {
		document.getElementById('workoutInfo').innerHTML = "";
	}
	var workoutItem;
	if (reps != 0) {
		text = document.getElementById('ChosenExercise').innerHTML + ": " + reps + " reps<br>";
	} else if (activity != "") {
		text = document.getElementById('ChosenExercise').innerHTML + ": " + activity + "<br>";
	} else {
		text = document.getElementById('ChosenExercise').innerHTML + ": " + hours + " hours, " + minutes + " minutes<br>";
	}
	document.getElementById('workoutInfo').innerHTML += text;
	hideRepModal();
}
function submitWorkout() {
	var user = getUserName();;

	if (confirm("Are you sure you want to submit?")) {
		//submit stuff to backend
		//Spotify URL (Sprint 2)
		var workoutName = document.getElementById('workoutName').value;
		if (workoutName == null || workoutName == "") {
			alert("You forgot to name your workout!");
			return;
		}
		var workoutDate = document.getElementById('workoutDate').value;
		var sqlString = toSQLDate(workoutDate);
		//Always returns false if you choose today, so subtract one day in milliseconds to correct
		if (new Date(sqlString).getTime() < new Date().getTime() - 86400000) {
			alert("Your workout is scheduled to be in the past");
			return;
		}
		var exercises = document.getElementById('workoutInfo').innerHTML.split("<br>");
		if (exercises[0] == "Your workout is currently empty" || exercises.length < 1) {
			alert("You have not added any exercises");
			return;
		}
		//Last value is an empty string so ignore
		var splitString;
		var jsonExercises = {};
		for (var i = 0; i < exercises.length - 1; i++) {
			splitString = exercises[i].split(":");
			splitString[0] = '"' + splitString[0] + '"';
			jsonExercises[splitString[0]] = splitString[1];
		}
		var tags = new Array();
		if (document.getElementById("showTags").innerHTML != "") {
			var tagArray = document.getElementById("showTags").innerHTML.split("<br>");
			for (var i = 1; i < tagArray.length; i++) {
				tags.push(tagArray[i]);
			}
		}
		var status = document.getElementById('workoutPrivacy').value;
		var workoutStatus = 2;
		if (status == "private") workoutStatus = 0;
		else if (status == "friend") workoutStatus = 1;
		else workoutStatus = 2;
		var package = JSON.stringify({
			//Bug, sends todays date always
			"date": toSQLDate(new Date()),
			"wName": workoutName, 
			"userName": user, 
			"url": document.getElementById("spotifyURL").value, 
			"status": workoutStatus, 
			"exercises": jsonExercises, 
			"tags": tags});
		if (!update) {
			console.log("Not Updating");
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() { 
			    if (this.readyState == 4 && this.status == 200) {
					location.reload();
				}
			}
			xhttp.open("PUT", ajaxURL + "/addWorkout", true);
			xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhttp.send(package);
		}
		else {
			console.log("Updating");
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() { 
			    if (this.readyState == 4 && this.status == 200) {
					update = false;
					location.reload();
				}
			}
			xhttp.open("PUT", ajaxURL + "/editWorkout", true);
			xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhttp.send(package);
		}

		//Reset workout info
		document.getElementById('workoutInfo').innerHTML = "Your workout is currently empty";
		document.getElementById('workoutName').value = "";
		document.getElementById('workoutDate').valueAsDate = new Date();
		document.getElementById('workoutPrivacy').value = "public";
		document.getElementById("showTags").innerHTML = "";
		document.getElementById("showTags").style.display = 'none';
	}
}
function showToast(toast) {
    var t = document.getElementById(toast);
    t.className = "show";
    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ t.className = t.className.replace("show", ""); }, 3000);
}
function addPlaylist() {
	var playlistURL = document.getElementById("spotifyURL").value;
	if (playlistURL == null || playlistURL == "" || !playlistURL.includes('https://open.spotify.com/playlist/')) {
		alert("Invalid Playlist");
		return;
	}
	showToast("spotifyToast");
	hideSpotifyModal();
}
function populateDropdown(dropdown) {
	var tableName;
	if (dropdown == "workoutDropdown") {
		tableName = "plannedWorkouts";
		emptyText = "Workouts";
	} else {
		tableName = "foodJournals";
		emptyText = "Food Journals";
	}
	var rows = document.getElementById(tableName).rows.length;
	var loadDropdown = document.getElementById(dropdown);
	for (var i = 0; i < rows; i++) {
		var opt = document.createElement("option");
		opt.value = document.getElementById(tableName).rows[i].innerHTML + "~?.+_%^" + document.getElementById(tableName).rows[i].id;
		opt.text = document.getElementById(tableName).rows[i].innerHTML;
		opt.id = document.getElementById(tableName).rows[i].title;
		loadDropdown.add(opt);
	}
}
//Used to clear dropdowns before repopulating
function removeOptions(dropdown) {
    var i;
    for(i = dropdown.options.length - 1 ; i >= 0 ; i--)
    {
        dropdown.remove(i);
    }
}
function removePrivacyOptions(dropdown, privacy) {
	//Clears and readds options, then removes those that do not belong in privacy category
	console.log(dropdown.id);
	removeOptions(dropdown);
	populateDropdown(dropdown.id);
	var emptyText;
	if (dropdown.id == "workoutDropdown") {
		emptyText = "Workouts";
	} else {
		emptyText = "Food Journals"
	}
	var i;
	for (i = dropdown.options.length - 1; i >= 0; i--) {
		if (dropdown.options[i].id != privacy) {
			dropdown.remove(i);
		}
	}
	if (dropdown.options.length < 1) {
		var opt = document.createElement("option");
		opt.value = "";
		opt.text = "No " + emptyText;
		dropdown.add(opt);		
	}
}
function getUserName() {
	var email = localStorage.getItem("email").split("@");
	return email[0];
}
function toSQLDate(date) {
	return new Date(date).toISOString().slice(0, 11).replace('T', ' ')
}
function getFoodInfo() {
		//prepare package for backend
		var user = getUserName();
		var foodName;
		var foodDate;
		//Edit
		console.log(document.getElementById("foodEditDiv").style.display);
		if (document.getElementById("foodEditDiv").style.display == 'block') {
			foodName = document.getElementById("toEditDeleteFoodName").innerHTML;
			foodDate = document.getElementById("toEditDeleteFoodDate").innerHTML;
			entry = document.getElementById("displayJournal").value;
		} else {
			foodName = document.getElementById('foodName').value;
			if (foodName == null || foodName == "") {
				alert("You forgot to name your journal!");
				return;
			}
			var foodDate = document.getElementById('foodDate').value;
			if (foodDate == null || foodDate == "") {
				alert("You forgot to date your journal!");
				return;
			}
			var entry = document.getElementById('foodJournalText').value;
		}
		var sqlString = toSQLDate(foodDate);

		if (entry == null || entry == "") {
			alert("You forgot to make an entry!");
			return;
		}
		var status = document.getElementById('foodPrivacy').value;
		var journalStatus = 2;
		if (status == "private") journalStatus = 0;
		else if (status == "friend") journalStatus = 1;
		else journalStatus = 2;
		var package = JSON.stringify({
			"date": sqlString, 
			"dName": foodName, 
			"userName": user, 
			"entry": entry, 
			"status": journalStatus,});

		return package;
}
function backToWorkouts() {
	//Reset workout info
	document.getElementById('workoutInfo').innerHTML = "Your workout is currently empty";
	document.getElementById('workoutName').value = "";
	document.getElementById('workoutDate').valueAsDate = new Date();
	document.getElementById('workoutPrivacy').value = "public";
	document.getElementById("showTags").innerHTML = "";
	document.getElementById("showTags").style.display = 'none';
	showHide('workoutDashboard', 'workoutDisplay');
}
