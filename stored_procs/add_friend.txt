CREATE DEFINER=`gymbuddyback`@`%` PROCEDURE `add_friend`(
	IN in_email VARCHAR(255),
    IN in_friendEmail VARCHAR(255)
)
BEGIN
	DECLARE EXIT HANDLER FOR 1452 SELECT 'Email doesn\'t exit.';
    
	if (select exists (select 1 from Friends where (email = in_email and friendEmail = in_friendEmail))) then
		select 'Request Exists!';

	else
		insert into Friends (
			email,
			friendEmail
		)
		values (
			in_email,
			in_friendEmail
		);
	END if;
END