CREATE DEFINER=`gymbuddyback`@`%` PROCEDURE `delete_workout`(
	IN in_wId INT(11)
)
BEGIN
	delete from Exercise
    where wId = in_wId;
    
    delete from Tags
    where wId = in_wId;
    
    delete from Workout
    where wId = in_wId;
END